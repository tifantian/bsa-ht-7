import { call, put, takeEvery} from "redux-saga/effects";
import axios from "axios";
import { fetchMessages as actionFetchMessages } from "../store/actionsCreater"

export function* fetchMessages() {
  try {
		const response = yield call(axios.get, `http://localhost:3001/messages`);
    const data = yield response.data;
		yield put({ type: "FETCH_MESSAGES_SUCCESS", payload: data });
	} catch (err) {
		console.log("Fetch message error: ", err.message);
	}
}

export function* addMessage(action) {
	const newMessage = { ...action.payload };
	try {
		yield call(axios.post, `http://localhost:3001/messages/`, newMessage);
		yield put(actionFetchMessages());
	} catch (err) {
		console.log("Add message error: ", err.message);
	}
}

export function* deleteMessage(action) {
	const id = action.payload;
	try {
		yield call(axios.delete, `http://localhost:3001/messages/${id}`);
		yield put(actionFetchMessages());
	} catch (err) {
		console.log("Delete message error: ", err.message);
	}
}

export function* updateMessage(action) {
	const { _id } = action.payload;
	const updatedMessage = action.payload;

	try {
		yield call(axios.put, `http://localhost:3001/messages/${_id}`, updatedMessage);
		yield put(actionFetchMessages());
	} catch (err) {
		console.log("Update message error:  ", err.message);
	}
}

export function* watchFetchMessages() {
  yield takeEvery("FETCH_MESSAGES", fetchMessages);
}

export function* watchAddMessages() {
  yield takeEvery("ADD_MESSAGE", addMessage);
}

export function* watchDeleteMessage() {
  yield takeEvery("DELETE_MESSAGE", deleteMessage);
}

export function* watchUpdateMessage() {
  yield takeEvery("UPDATE_MESSAGE", updateMessage);
}
