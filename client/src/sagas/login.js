import { call, put, takeEvery } from "redux-saga/effects";

export function* login(credentials) {
  try {
    const response = yield call(loginApi, credentials);
    yield put({ type: "LOGIN_SUCCESS", payload: response });
  } catch (error) {
    console.log("Login error:", error.message);
  }
}

const loginApi = async credentials => {
  const users = await fetch("http://localhost:3001/user/login", {
    method: "post",
    body: JSON.stringify(credentials.payload),
    headers: { "Content-Type": "application/json" }
  });
  return users.json();
};

export function* watchLogin() {
  yield takeEvery("LOGIN", login);
}
