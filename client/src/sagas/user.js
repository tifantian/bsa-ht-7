import { call, put, takeEvery} from "redux-saga/effects";
import axios from "axios";
import { fetchUsers as actionFetchUsers } from '../store/actionsCreater';
import { getUserSuccess } from '../store/actionsCreater'


export function* fetchUsers() {
	
  try {
		const response = yield call(axios.get, `http://localhost:3001/user`);
		const data = yield response.data;
		yield put({ type: "FETCH_USERS_SUCCESS", payload: data });
	} catch (err) {
		console.log("Get users error: ", err.message);
	}
}

export function* deleteUser(action) {
	const id = action.payload;

	try {
		yield call(axios.delete, `http://localhost:3001/user/${id}`);
		yield put(actionFetchUsers());
	} catch (err) {
		console.log("Delete user error: ", err.message);
	}
}

export function* getUser(action) {

	try {
		const response = yield call(axios.get, `http://localhost:3001/user/${action.payload}`);
		const data = yield response.data;
		yield put(getUserSuccess(data));
	} catch (err) {
		console.log("Get user error", err.message);
	}
}

export function* updateUser(action) {
	const id = action.payload.id;
	const updatedUser = { ...action.payload.userObject };

	try {
		yield call(axios.put, `http://localhost:3001/user/${id}`, updatedUser);
		yield put(actionFetchUsers());
	} catch (err) {
		console.log("Update user error: ", err.message);
	}
}

export function* addUser(action) {
	const newUser = { ...action.payload };

	try {
		yield call(axios.post, `http://localhost:3001/user/`, newUser);
		yield put(actionFetchUsers());
	} catch (err) {
		console.log("Add user error: ", err.message);
	}
}

export function* watchFetchUsers() {
  yield takeEvery("FETCH_USERS", fetchUsers);
}

export function* watchDeleteUser() {
  yield takeEvery("DELETE_USER", deleteUser);
}

export function* watchGetUser() {
	yield takeEvery("GET_USER", getUser);
}

export function* watchUpdateUser() {
	yield takeEvery("UPDATE_USER", updateUser);
}

export function* watchAddUser() {
	yield takeEvery("ADD_USER", addUser);
}


