import { all } from "redux-saga/effects";
import { 
  watchFetchUsers, 
  watchDeleteUser, 
  watchGetUser, 
  watchUpdateUser, 
  watchAddUser } from './user';
import { watchLogin } from './login';
import { 
  watchFetchMessages, 
  watchAddMessages, 
  watchDeleteMessage, 
  watchUpdateMessage } from './messages';


export default function* sagas() {
  yield all([
    watchFetchMessages(),
    watchFetchUsers(),
    watchLogin(),
    watchDeleteUser(),
    watchGetUser(),
    watchUpdateUser(),
    watchAddUser(),
    watchAddMessages(),
    watchDeleteMessage(),
    watchUpdateMessage()
  ]);
}
