import { LOGIN_SUCCESS } from '../actionsList';

const initialState = {
  currentUser: undefined,
  loginSuccess: null,
  isAdmin: null
}

export const loginReduser = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS:
      const { loginSuccess } = action.payload;
      if (loginSuccess) {
        const { currentUser } = action.payload;
        return {
          ...state,
          currentUser: currentUser,
          isAdmin: currentUser.isAdmin,
          loginSuccess: true
        }
      } else {
        return {
          ...state,
          currentUser: null,
          isAdmin: null,
          loginSuccess: false
        }
      }
    default:
      return state;
  }
}

