import { FETCH_USERS_SUCCESS, DELETE_USER, GET_USER_SUCCESS } from "../actionsList";

const initialState = {
  users: []
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {

    case FETCH_USERS_SUCCESS: 
      return action.payload;

    case DELETE_USER:
      return action.payload

    case GET_USER_SUCCESS:
      return {
        user: action.payload
      }

    default:
      return state;
  }
}