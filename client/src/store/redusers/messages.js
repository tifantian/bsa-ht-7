import { 
  ADD_MESSAGE, 
  DELETE_MESSAGE, 
  UPDATE_MESSAGE, 
  FETCH_MESSAGES_SUCCESS} from "../actionsList";

const initialState = {
  messages: []
};

export const messagesReducer = (state = initialState, action) => {
  
  switch (action.type) {

    case FETCH_MESSAGES_SUCCESS:
      return action.payload;

    case ADD_MESSAGE:
      return [...state, action.payload];

    case DELETE_MESSAGE:
      return [...state].filter(message => message.id !== action.payload);

    case UPDATE_MESSAGE:
      return action.payload;

    default:
      return state;
  }
}