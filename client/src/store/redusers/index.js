import { combineReducers } from "redux";
import { messagesReducer } from "./messages";
import { userReducer } from './user';
import { loginReduser } from '../redusers/login';

export default combineReducers({
  messages: messagesReducer,
  users: userReducer,
  login: loginReduser
});
