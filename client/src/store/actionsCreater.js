import { 
  FETCH_MESSAGES,
  ADD_MESSAGE, 
  DELETE_MESSAGE, 
  UPDATE_MESSAGE, 
  FETCH_USERS, 
  LOGIN, 
  DELETE_USER, 
  GET_USER, 
  GET_USER_SUCCESS, 
  UPDATE_USER, 
  ADD_USER } from './actionsList';

export const fetchMessages = ()  => ({
  type: FETCH_MESSAGES
});

export const fetchUsers = () => ({
  type: FETCH_USERS
});

export const loginAction = userInfo => ({
  type: LOGIN,
  payload: userInfo
});

export const getUser = (id) => {
	return {
		type: GET_USER,
		payload: id
	};
};

export const getUserSuccess = (user) => {
	return {
		type: GET_USER_SUCCESS,
		payload: user
	};
};

export const addMessage = (newMessage) => {
  return {
    type: ADD_MESSAGE,
    payload: newMessage
  }
};

export const deleteMessage = (id) => {
  return {
    type: DELETE_MESSAGE,
    payload: id
  }
}

export const updateMessage = (objectMessage) => {
  return {
    type: UPDATE_MESSAGE,
    payload: objectMessage
  }
}

export const deleteUser = (id) => {
  return {
    type: DELETE_USER,
    payload: id
  }
}

export const updateUser = (id, userObject) => {
  return {
    type: UPDATE_USER,
    payload: { id, userObject }
  }
}

export const addUser = (userObject) => {
  return {
    type: ADD_USER,
    payload: userObject
  }
}