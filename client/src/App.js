import React from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import history from "./history";
import Users from "./components/Users/Users"
import Login from "./components/Login/Login"
import "./reset.css";
import UserModal from './components/UserModal/UserModal';
import { connect } from "react-redux";
import Chat from "./components/Chat/ChatMain";

class App extends React.Component {
  render() {
    const { currentUser, isAdmin, loginSuccess } = this.props.login;
    return (
      <div className="App">
        <Router history={history}>
          {currentUser && isAdmin && loginSuccess ? (<Redirect to='/users' />) : null}
          {currentUser && !isAdmin  ? (<Redirect to='/Chat' />) : null}

          <Switch>
            <Route path="/Chat" exact component={Chat} />
            <Route path="/users" exact component={Users} />
            <Route path="/login" exact component={Login} />
            <Route path="/user/:id" exact component={UserModal} />
            <Redirect to="/login"  />
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
	return {
		login: state.login
	};
};

export default connect(mapStateToProps)(App);

