import React from 'react'; 
import Message from './Message'

class MessageList extends React.Component {

  getAllMessages() {
    let keyForMessage = 0;
    const allMesagesArray = this.props.messages.map( (oneMess) => {
      return <Message 
        currentUser = {this.props.currentUser} 
        oneMessage={oneMess} 
        key={keyForMessage++} 
        onMessageDelete={this.props.onMessageDelete} 
        onMessageUpdate={this.props.onMessageUpdate}
      />;
    });
    return allMesagesArray;
  }

  render() {
    if(!this.props.currentUser) {
      return null;
    }
    return (
      <main className="message-list"> 
        {this.getAllMessages()}
      </main>
    )
  }
}

export default MessageList;