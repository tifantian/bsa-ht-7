import React from 'react';
import getDateString from '../helpers/date'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Form } from 'reactstrap';

class Message extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      like: false,
      modal: false,
      unmountOnClose: false,
      bodyMessage: this.props.oneMessage.message
    }

    this.currentUserName = this.props.currentUser.name;
    this.isAdmin = this.props.currentUser.isAdmin;
    this.toggle = this.toggle.bind(this);
  }

  handleUpdMessage(ev) {
    this.toggle();
    const newObjectMessage = { ...this.props.oneMessage, message: this.state.bodyMessage };
    this.props.onMessageUpdate(newObjectMessage);
  }

  handleDeleteMessage() {
    const { oneMessage, onMessageDelete } = this.props;
    onMessageDelete(oneMessage._id);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  getMessage() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? objectMessage.message : null;
  }

  getLogoSrc() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? objectMessage.avatar : null;
  }

  getDate() {
    const objectMessage = this.props.oneMessage;
    const date = (objectMessage) ? objectMessage.created_at : null;
    return getDateString(date);

  }

  getAltPropForImg() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? `photo-of-${objectMessage.user}` : null;
  }

  getAuthor() {
    const objectMessage = this.props.oneMessage;
    return (objectMessage) ? objectMessage.user : null;
  }

  render() {
    const { like } = this.state;
    if (!this.currentUserName) {
      return null;
    }
    let allowDeleteMessage = false;
    const author = this.getAuthor();
    if (author === this.currentUserName) {
      allowDeleteMessage = true;
    }
    if (this.isAdmin) {
      allowDeleteMessage = true;
    }

    return (
      <div className={this.getAuthor() === this.currentUserName ? "main-message my-message" : "main-message"}>
        <div className="message-logo">
          <img src={this.getLogoSrc()} alt={this.getAltPropForImg()} />
        </div>
        <div className="message-info">
          <div className="message-info-upper">
            <div className="message-author">
              {this.getAuthor()}
              {(this.props.oneMessage.user === "admin") ? "★" : null}
            </div>
            <div className="message-date">
              {this.getDate()}
            </div>
          </div>
          <div className="message-text">
            {this.getMessage()}
          </div>
          <div className="message-info-bottom">
            <div className="message-like">
              <div onClick={ev => this.setState({ like: !this.state.like })}>
                {like && this.props.oneMessage.user !== this.currentUserName ? 1 : 0} {' '} likes
              </div>
            </div>
            <div className='message-info-bottom-right'>
              {this.props.oneMessage.user === this.currentUserName &&
                <div className="message-info-edit-comment">
                  <Form inline onSubmit={(e) => e.preventDefault()}>
                    <div className='message-edit' onClick={this.toggle}>Edit</div>
                  </Form>
                  <Modal isOpen={this.state.modal}
                    toggle={this.toggle}
                    className={this.props.className}
                    unmountOnClose={this.state.unmountOnClose}>
                    <ModalHeader toggle={this.toggle}>Edit message</ModalHeader>
                    <ModalBody>
                      <Input type="textarea"
                        value={this.state.bodyMessage}
                        rows={5}
                        onChange={e => this.setState({ ...this.state, bodyMessage: e.target.value })} />
                    </ModalBody>
                    <ModalFooter>
                      <Button color="secondary"
                        onClick={ev => this.handleUpdMessage(ev)}>Edit
                  </Button>{' '}
                      <Button color="primary"
                        onClick={this.toggle}>Cancel
                  </Button>
                    </ModalFooter>
                  </Modal>
                </div>}
              <div onClick={e => this.handleDeleteMessage()}
                className={
                  allowDeleteMessage ? "delete-message-my " : "delete-message "}>
                Delete
            </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Message;
