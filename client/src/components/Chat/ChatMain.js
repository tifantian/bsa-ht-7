import React from "react";
import Header from './Header/Header';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import LoadingPage from '../LoadingPage';
import { Redirect, Link } from "react-router-dom";

import './stylesChat/styles.css';
import { connect } from 'react-redux';
import { fetchMessages, addMessage, deleteMessage, updateMessage, fetchUsers } from '../../store/actionsCreater'

class Chat extends React.Component {

  componentDidMount() {
    this.props.fetchMessages();
    this.props.fetchUsers();
  }
  
  render() {
    const {messages, login, users} = this.props.totalData;
    if (messages.length) {
      return (
        <div className="Chat">
          {(login.loginSuccess !== true) ? <Redirect to='/login' /> : null}
          {
            (login.isAdmin === true) ?
              (
                <Link to='/users'>
                  <button className="nav-button">Users</button>
                </Link>
              ) : null 
          }
          <Link to="/login">
            <button className="nav-button">
              Exit to login
            </button> 
          </Link>
          <Header 
            messages={messages}
            usersLength={users.length} 
            currentUser={login.currentUser}
          />
          <MessageList
            messages={messages}
            onMessageUpdate={(objectMessage) => this.props.updateMessage(objectMessage)}
            onMessageDelete={(id) => this.props.deleteMessage(id)}
            currentUser={login.currentUser}
          />
          <MessageInput 
            onMessageSend={(messageObject) => this.props.addMessage(messageObject)}
            currentUser={login.currentUser}  
          />
        </div>
      );
    } else {
      return <LoadingPage />
    }
  }
}

const putStateToProps = state => {
  return {
    totalData: state,
  }
};

const putDispatchToProps = {
  fetchMessages,
  addMessage,
  deleteMessage,
  updateMessage,
  fetchUsers
}

export default connect(putStateToProps, putDispatchToProps)(Chat);