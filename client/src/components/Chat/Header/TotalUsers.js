import React from 'react';

class TotalUsers extends React.Component {

  render() {
    return (
      <div className='header-total-users'>
        {this.props.usersLength} users
      </div>
    )
  }
}

export default TotalUsers;