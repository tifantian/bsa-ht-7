import React from "react";
import { connect } from "react-redux";
import "./stylesUserModal/style.css"
import { getUser } from "../../store/actionsCreater";
import { updateUser, addUser } from "../../store/actionsCreater"

class UserModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      _id: "",
      name: "",
      email: "",
      isAdmin: "",
      password: "",
      image: "",
    }
  }

  componentDidMount() {
    const { getUser, match } = this.props;
    if (parseInt(match.params.id) > 0) {
      getUser(match.params.id);
    }
    if (match.params.id === "new") {
      this.setState({ ...this.state, _id: 'new' });
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.match.params.id && props.user && props.user._id !== state._id && state._id !== 'new') {
      return {
        ...props.user
      };
    } else {
      return null;
    }
  }

  onClickEdit() {
    const { history } = this.props;
    if (!isNaN(this.state._id)) {
      this.props.updateUser(this.state._id, this.state);
    }
    if (this.state._id === 'new') {
      this.props.addUser(this.state);
    }
    history.goBack();
  }

  onClickCancel() {
    const { history } = this.props;
    history.goBack();
  }

  render() {
    if (!this.props.user && this.state._id !== "new") {
      return null;
    }
    const { name, email, password } = this.state;
    return (
      <div className="user-modal-page">
        <div className="user-modal">
          <div className="modal-top">
            <div className="modal-name">
              Input info for user
            </div>
            <div className="modal-top-line">

            </div>
            <input className="modal-user-name"
              type="text"
              value={name}
              onChange={(ev) => this.setState({ ...this.state, name: ev.target.value })}
              placeholder="name"
            />
            <input className="modal-user-email"
              type="text"
              value={email}
              onChange={(ev) => this.setState({ ...this.state, email: ev.target.value })}
              placeholder="email"
            />
            <input className="modal-user-password"
              type="password"
              value={password}
              onChange={(ev) => this.setState({ ...this.state, password: ev.target.value })}
              placeholder="password"
            />

          </div>
          <div className="modal-buttons">
            <button className="modal-edit"
              onClick={() => this.onClickEdit()}
            >
              Confirm
            </button>
            <button className="modal-cancel"
              onClick={() => this.onClickCancel()}
            >
              Cancel
            </button>
          </div>

        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.users.user
  };
};

const mapDispatchToProps = {
  getUser,
  updateUser,
  addUser
};

export default connect(mapStateToProps, mapDispatchToProps)(UserModal);