import React from "react";
import { connect } from 'react-redux';
import "./loginStyles/styles.css"
import { loginAction } from '../../store/actionsCreater';

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      login: "",
      password: ""
    }
  }

  componentDidMount() {
    this.nameInput.focus();
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.loginAction({ name: this.state.login, password: this.state.password });
  }

  render() {
    const { loginSuccess } = this.props.login;
    return (
      <div className="login-page">
        <form className="login-form"> 
          <label htmlFor="login-field">Login</label>

          <input 
              className="user-login-input"
              type="text"
              id="login-field"
              placeholder="your login"
              ref={input => {
                this.nameInput = input;
              }}
              value={this.state.login}
              onChange={(event) => this.setState({ login: event.target.value })}
            />
          
          <label htmlFor="password-field">Password</label>

          <input 
              className="user-password-input"
              type="password"
              id="password-field"
              placeholder="your password"
              value={this.state.password}
              onChange={(event) => this.setState({ password: event.target.value })}
            />

          <input className="button-submit"
            type="submit"
            value="submit"
            onClick={(ev) => this.handleSubmit(ev)}
          />
          <div className="warning-message">
            {(loginSuccess === false) ? (<span>Authorization failed</span>) : null}  
          </div>   
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    login: state.login
  };
};

export default connect(mapStateToProps, { loginAction } )(Login);