import React from "react";

class UserItem extends React.Component {
  
  onEdit(id) {
    this.props.history.push(`/user/${id}`);
  }

  onDelete() {
    const { user, deleteUser } = this.props;
    deleteUser(user._id);
  }

  render() { 
    const { user } = this.props;
    return (
      <div className="user-item">
        <div className='user-info'> 
          <div className="user-email">
            <span>{user.email}</span>
          </div>
          <div className="user-name">
            <span>{user.name}</span>
          </div>
        </div>
        <div className="user-buttons">
          <button 
            onClick={() => this.onEdit(user._id)}
            className="btn-edit">
            edit
          </button>
          <button
            onClick={() => this.onDelete()} 
            className="btn-delete">
              delete
          </button>
        </div>
      </div>
    )
  }
}

export default UserItem;