import React from "react";
import { connect } from 'react-redux';
import UserItem from './UserItem';
import LoadingPage from '../LoadingPage';
import './usersStyles/styles.css';
import { Link } from "react-router-dom";

import { fetchUsers, deleteUser } from '../../store/actionsCreater'

class Users extends React.Component {
  componentDidMount() {
    this.props.fetchUsers();
  }

  renderUsers() {
    const { users, history } = this.props;
    if (!users[0] || users.length === 0 || !isNaN(users)) {
      return <LoadingPage />
    }  
    const usersElements = users.map((user) => 
      <UserItem 
        deleteUser={(id) => this.props.deleteUser(id)} 
        key={user._id} 
        user={user} 
        history={history}
    />);
    return usersElements;
  }

  onClickExit() {
    this.props.history.push('/login');
  }

  onClickAddUser() {
    this.props.history.push(`/user/new`);
  }

  render() {
    return (
      <div className="user-page">
        <div className="users-navigation">
          <button 
            className='nav-button' 
            onClick={() => this.onClickAddUser()}>
            Add user
          </button>
          <Link to='/login'>
            <button className='nav-button'>
              Exit to login
            </button>
          </Link>
          <Link to='/chat'>
            <button className='nav-button'>
              Chat
            </button>
          </Link>
        </div>
        <div className='user-list'>
          {this.renderUsers()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    users: state.users,
  };
};

export default connect(
  mapStateToProps,
  { fetchUsers, deleteUser }
)(Users);