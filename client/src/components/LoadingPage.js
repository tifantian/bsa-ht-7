import React from 'react';

class LoadingPage extends React.Component {

  render() {
    return (
      <div className="loading-page">Loading...</div>
    )
  }

}

export default LoadingPage;