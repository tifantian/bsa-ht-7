/* services/user.service.js */
const repository = require("../repositories/user.repository");

const getAllUsers = () => {
  return repository.getUserList();
}

const checkUser = (userInfo) => {
  const { password, name } = userInfo;
  return repository.checkUserData(password, name);
}

const getUser = (id) => {
  if(isNaN(id)) {
    return false;
  }
  return repository.getUserById(id);
}

const deleteUser = (id) => {
  if(isNaN(id)) {
    return false;
  }
  return repository.deleteUserById(id);
}

const addNewUser = (userObject) => {
  if(
    userObject.hasOwnProperty('name') &&
    userObject.hasOwnProperty('email') &&
    userObject.hasOwnProperty('password') &&
    userObject.hasOwnProperty('isAdmin') &&
    userObject.hasOwnProperty('image')
  ) {
    return repository.saveNewUser(userObject);
  }
  return false;
}

const putUser = (userObject, id) => {
  if(isNaN(id)) {
    return false;
  }
  if(
    userObject.hasOwnProperty('name') &&
    userObject.hasOwnProperty('email') &&
    userObject.hasOwnProperty('password') &&
    userObject.hasOwnProperty('isAdmin') &&
    userObject.hasOwnProperty('image')
  ) {
    return repository.saveChangesToUser(userObject, id);
  }
  return false;
}

module.exports = {
  getAllUsers,
  checkUser,
  getUser,
  deleteUser,
  addNewUser,
  putUser
};