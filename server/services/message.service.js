const repository = require("../repositories/message.repository");

const getAllMessages = () => {
  return repository.getMessageList();
}

const deleteMessage = (id) => {
  return repository.deleteMessageById(id);
}

const addNewMessage = (messageObject) => {
  if(
    messageObject.hasOwnProperty('user') &&
    messageObject.hasOwnProperty('avatar') &&
    messageObject.hasOwnProperty('created_at') &&
    messageObject.hasOwnProperty('message') &&
    messageObject.hasOwnProperty('marked_read') &&
    messageObject.hasOwnProperty('_id')
  ) {
    return repository.saveNewMessage(messageObject);
  }
  return false;
}

const putMessage = (messageObject, id) => {
  if(
    messageObject.hasOwnProperty('user') &&
    messageObject.hasOwnProperty('avatar') &&
    messageObject.hasOwnProperty('created_at') &&
    messageObject.hasOwnProperty('message') &&
    messageObject.hasOwnProperty('marked_read')
  ) {
    return repository.saveChangesToMessage(messageObject, id);
  }
  return false;
}

module.exports = {
  getAllMessages,
  deleteMessage,
  addNewMessage,
  putMessage
}
