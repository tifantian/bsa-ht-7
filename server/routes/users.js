/* routes/users.js */

var express = require('express');
var router = express.Router();


const service = require("../services/user.service");

// get All users
router.get('/', function (req, res) {
  const result = service.getAllUsers();
  res.send(result);
});

// get user by ID
router.get('/:id', function (req, res) {
  const id = req.params.id;
  const result = service.getUser(id);
  if (result) {
    res.send(result);
  } else {
    res.status(401).send(`User with ${id} does not exist`);
  }
});

// delete user by id
router.delete('/:id', function (req, res) {
  const id = req.params.id;
  const result = service.deleteUser(id);
  if (result) {
    res.send(`user with ${id} id was deleted`);
  } else {
    res.status(401).send(`user with ${id} id does not exist`);
  }
});

// login user: login true of false
router.post('/login', function (req, res) {
  const result = service.checkUser(req.body);
  if (result) {
    res.send( { 
      loginSuccess: true,
      currentUser: result 
    });
  } else {
    res.status(401).send({ loginSuccess: false});
  }
});

// post new User to repository
router.post('/', function (req, res) {
  const result = service.addNewUser(req.body);
  if (result) {
    res.send({ id: result }); // return new id to client
  } else {
    res.status(401).send('wrong format of new User');
  }
});

// change user data
router.put('/:id', function (req, res) {
  const result = service.putUser(req.body, req.params.id);
  if (result) {
    res.send(`Changes save`);
  } else {
    res.status(401).send('wrong format');
  }
})

module.exports = router;