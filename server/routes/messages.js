var express = require('express');
var router = express.Router();


const service = require("../services/message.service");

// get All messages
router.get('/', function (req, res) {
  const result = service.getAllMessages();
  res.send(result);
});

// delete message by id
router.delete('/:id', function (req, res) {
  const id = req.params.id;
  const result = service.deleteMessage(id);
  if (result) {
    res.send(`message with ${id} id was deleted`);
  } else {
    res.status(401).send(`message with ${id} id does not exist`);
  }
});

// post new Message to repository
router.post('/', function (req, res) {
  const result = service.addNewMessage(req.body);
  if (result) {
    res.send("Success"); 
  } else {
    res.status(401).send('wrong format of new Message');
  }
});

//put Message to repo
router.put('/:id', function (req, res) {
  const result = service.putMessage(req.body, req.params.id);
  if(result) {
    res.send(`Changes save`);
  } else {
    res.status(401).send('wrong format');
  }
})

module.exports = router;