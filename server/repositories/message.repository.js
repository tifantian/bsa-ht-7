const fs = require('fs'); 
const pathToFile = './data/messageList.json';
const file = fs.readFileSync(pathToFile, 'utf-8');
let messageList = JSON.parse(file); 

function saveChangesToFile(pathToFile, userList) { 
  const newFile = JSON.stringify(userList);
  fs.writeFileSync(pathToFile, newFile, 'utf-8');
}

const getMessageList = () => {
  return messageList;
};

const deleteMessageById = (id) => {
  const index = messageList.findIndex(element => element._id == id);
  if(index === -1) {
    return false;
  }
  messageList.splice(index, 1);
  saveChangesToFile(pathToFile, messageList);
  return true;
}

const saveNewMessage = (messageObject) => {
  messageList.push(messageObject);
  saveChangesToFile(pathToFile, messageList);
  return true;
}

const saveChangesToMessage = (messageObject, id) => {
  const index = messageList.findIndex(element => element._id == id);
  if(index >= 0) { // 1
    Object.assign(messageList[index], messageObject);
    messageList[index]._id = id;
    saveChangesToFile(pathToFile, messageList);
    return true;
  }
  return false;
}

module.exports = {
  getMessageList,
  deleteMessageById,
  saveNewMessage,
  saveChangesToMessage
}

