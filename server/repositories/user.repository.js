/* repositories/user.repository.js */

const fs = require('fs'); 
const pathToFile = './data/userlist.json';
const file = fs.readFileSync(pathToFile, 'utf-8');
let userList = JSON.parse(file); 


function saveChangesToFile(pathToFile, userList) { 
  const newFile = JSON.stringify(userList);
  fs.writeFileSync(pathToFile, newFile, 'utf-8'); 
}

const getUserList = () => {
  return userList;
};

const checkUserData = (password, name) => {
  const user = userList.find(item => item.name == name && item.password == password);
  if(user) {
    return user;
  } 
  return false;
}

const getUserById = (id) => {
  return userList.find(element => element._id == id);
}

const deleteUserById = (id) => {
  const index = userList.findIndex(element => element._id == id);
  if(index === -1) {
    return false;
  }
  userList.splice(index, 1);
  saveChangesToFile(pathToFile, userList);
  return true;
}

const saveNewUser = (userObject) => {
  let newID;
  if(userList.length === 0) {
    newID = 1;
  } else {
    newID = Math.max(...userList.map((user) => user._id)) + 1;
  } 
  userObject.avatar = `https://i.pravatar.cc/300?img=${newID}`;
  if(userObject.isAdmin === "") {
    userObject.isAdmin = false;
  }
  userObject._id = newID;
  userList.push(userObject);
  saveChangesToFile(pathToFile, userList);
  return newID;
}

const saveChangesToUser = (userObject, id) => {
  const index = userList.findIndex(element => element._id == id);
  if(index >= 0) {
    userList[index]._id = id;
    Object.assign(userList[index], userObject);
    saveChangesToFile(pathToFile, userList);
    return id;
  }
  return false;
  
}

module.exports = {
  getUserList,
  checkUserData,
  getUserById,
  deleteUserById,
  saveNewUser,
  saveChangesToUser
};