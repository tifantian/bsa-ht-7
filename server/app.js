var express = require('express'); 
var cookieParser = require('cookie-parser');
var morgan = require('morgan'); 
var cors = require('cors'); 

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var messageRouter = require('./routes/messages');

var app = express();

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/messages', messageRouter);

module.exports = app;
